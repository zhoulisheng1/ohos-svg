# ohos-svg

## 简介

ohos-svg是一个SVG图片的解析器和渲染器，可以解析SVG图片并渲染到页面上，还可以动态改变SVG的样式。

## 效果展示
SVG图片解析并绘制:

![](screenshot/example1.png)


## 下载安装

```shell
ohpm install @ohos/svg
```

## 使用说明

 ```
import { SVGImageView } from '@ohos/svg'
 
 model: SVGImageView.SVGImageViewModel = new SVGImageView.SVGImageViewModel();
 
 build() {
   SVGImageView({ model: this.model })
 }
 ```

## 接口说明
`model: SVGImageView.SVGImageViewModel = new SVGImageView.SVGImageViewModel();`
1. 设置svg资源文件
   `this.model.setImageRawfile(filename: string)`
2. 设置svg对象
   `this.model.setSVG(svg: SVG, css?: string)`
3. 设置svg资源图片
   `this.model.setImageResource(resource: Resource)`
4. 设置svg图片的源文件字符串
   `this.model.setFromString(url: string)`

## 约束与限制
在下述版本验证通过：

DevEco Studio: 3.1Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## 目录结构
````
|---- ohos-svg  
|     |---- entry  # 示例代码文件夹
|     |---- ohos_svg  # ohos_svg库文件夹
|           |---- index.ets  # 对外接口
            |---- components  # 组件代码目录
                  |---- CSS.ets
                  |---- PreserveAspectRatio.ets
                  |---- RenderOptions.ets
                  |---- SimpleAssetResolver.ets
                  |---- SVG.ets
                  |---- SVGExternalFileResolver.ets
                  |---- SVGImageView.ets
                  |---- SVGParseException.ets
                  |---- SVGImageView.ets
                        |---- utils  
                              |---- Character  
                              |---- CSSBase  
                              |---- CSSFontFeatureSettings  
                              |---- CSSFontVariationSettings  
                              |---- CSSTextScanner  
                              |---- IntegerParser  
                              |---- Matrix  
                              |---- mini_canvas  
                              |---- NumberParser  
                              |---- Rect  
                              |---- RenderOptionsBase  
                              |---- Style  
                              |---- SVGBase  
                              |---- SVGParser  
                              |---- SVGParserImpl  
                              |---- SVGRenderer  
                              |---- SVGXMLChecker  
                              |---- SVGXMLConstants  
                              |---- TextScanner  

|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos-svg/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/ohos-svg/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/ohos-svg/blob/master/LICENSE) ，请自由地享受和参与开源。

## 遗留问题

1.目前mask标签绘制有问题

2.svg图片含有image标签时需要将svg图片和image标签引用的图片共同放在rawfile文件夹下