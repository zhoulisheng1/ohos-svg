## v2.0.0

- 包管理工具由npm切换为ohpm
- 适配DevEco Studio: 3.1Beta2(3.1.0.400)
- 适配SDK: API9 Release(3.2.11.9)
## v1.1.0

- 重构canvas解析绘制逻辑，并增加renderDocument接口
- 目前除mask以外，其他的svg组件基本支持
- 注：当svg里有image标签使用时必须将此svg和image标签引用的image放入rawfile下才能正常绘制

## v1.0.2

1. 调整getfromstring方法名
2. 解决watch监听abouttoappear不会重新走
3. svg类型拆分
4. 补充了canvas绘制逻辑
5. 补充了text、switch以及use标签解析绘制

## v1.0.1

api8升级到api9，并转换为stage模型

## v1.0.0

- ohos-svg是一个解析渲染SVGLibrary，支持了以下功能：
1. 解析SVG
2. 渲染SVG
3. 改变SVG样式

- 不支持以下功能：
1. svg字体解析绘制